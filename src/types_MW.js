/**
 * Содержит описание объектов возвращаемых JSON API "Мой склад"
 *
 * @namespace MW
 */

/**
 * Данныы JSON API "Мой склад" для документов содержащихся в поле attributes
 *
 * @typedef {Object} MW.metadata
 * @property {MW.meta} Метаданные
 * @property {string} id: "4491c608-1843-11ea-0a80-0169000af407",
 * @property {string} accountId": "4671d786-5167-11e9-9109-f8fc00003482",
 * @property {string} updated": "2019-12-06 19:12:58.048",
 * @property {string} name": "1. Создать ТЗ",
 * @property {string} externalCode": "M7hR3gLnhAKHk7xHfvNNM3"
 */

/**
 * @typedef {Object} MW.meta
 * @property {string} href
 * @property {string} metadataHref
 * @property {string} type
 * @property {string} mediaType
 * @property {string} uuidHref
 */