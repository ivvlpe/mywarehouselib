/**
 * @file Получает сообщения от сервиса мой склад и отправляет их
 * передает их в бот Telegram для отправки
 */

const MyWarehouse = {};

/**
 *
 * @param {Object} config
 * @param {string} config.host
 * @param {string} config.port
 * @param {string} config.httpsOptions
 * @param {Object} [config.endpoints]
 * @param {Logger} config.lg
 */
MyWarehouse.init = function init(config) {
    if (!this.isInit) {
        MyWarehouse.host = config.host;
        MyWarehouse.port = config.port;
        MyWarehouse.endpoints = config.endpoints;
        MyWarehouse.httpsOptions = config.httpsOptions;
        MyWarehouse.lg = config.lg;
        MyWarehouse.helper = require('helper')({
            lg: config.lg.child({module: 'helper'})
        });
        this.isInit = true;
    }else{
        this.lg.warn(`Module init already`);
    }
};

const https = require('https');


/**
 * Устанавливает webhook для получения информации
 * об изменении сотрудника
 */

/**
 * Устанавливает webhook для получения информации
 * об изменении сотрудника
 * @alias {MyWarehouse.setWebHookUpdateEmployee}
 */
MyWarehouse.setWebHookUpdateEmployee = async function setWebHookUpdateEmployee() {
    function getPayload() {
        const all = ['UPDATE', 'CREATE', 'DELETE'];
        const endpoint = MyWarehouse.endpoints.employee_update;
        const entityType = "employee";
        return all.map(action => {
            return {endpoint, entityType, action};
        })
    }

    this.lg.silly('Start setWebHookUpdateEmployee');
    const payloads = getPayload();
    payloads.forEach(e => {
        this.setWebHook(e.action, e.entityType, e.endpoint)
    });
};


/**
 * Устанавливает webhook
 * Подробности https://dev.moysklad.ru/doc/api/remap/1.2/#%D0%B2%D0%B5%D0%B1-%D1%85%D1%83%D0%BA%D0%B8
 *
 * @param {string} action дейстивие при котором будет производиться запрос
 *  Возможные значения: [CREATE, UPDATE, DELETE]
 *
 * @param entityType -  тип сущности, к которой привязан веб-хук
 * @param endpoint - путь относительно текущего сервера куда будут приходить запросы
 */
MyWarehouse.setWebHook = function setWebHook(action, entityType, endpoint) {
    this.lg.silly(`Start setWebHook for action: "${action}", entityType: "${entityType}"`);
    const payload = {
        url: 'https://' + this.host + ':' + this.port + endpoint,
        action,
        entityType
    };
    const options = this.getOptions('POST', 'entity/webhook');
    /*
    Получаем данные об установленных вебхуках, если есть - изменяем
    в противном случае создаем
     */
    this.getWebHooks().then(data => {
        // noinspection JSUnresolvedVariable
        if (data.errors) {
            const error = data.errors[0];
            let message = `I cant't get info about webhooks on My Warehouse: Code: ${error.code}` +
                `Description: ${error.error}`;
            process.exitCode = 1;
            throw new Error(message);
        }
        const item = data.rows.filter(i => i.entityType === entityType && i.action === action);
        if (item.length > 0) {
            options.path += `/${item[0].id}`;
            options.method = 'PUT'
        }
        this.send(options, JSON.stringify(payload), (err, data) => {
            if (err) {
                this.lg.error(`I cant set webhooks "${action} ${entityType}" ${this.helper.errToString(err)}`);
            } else {
                this.lg.info(`Webhook "${action} ${entityType}" is setup to "${data.url}"`);
            }
        });
    }, reason => {
        this.lg.error(`I can't get info about webhooks on My Warehouse ${this.helper.errToString(reason)}`)
    });
};

/**
 * Устанавливает получение вебхука от "Мой склад" при создании "Заказ на производство"
 */
MyWarehouse.setWebHookCreateProcessingorder = function setWebHookCreateProcessingorder() {
    this.setWebHook('CREATE', 'processingorder', this.endpoints.processingorder_create)
};

/**
 * Устанавливает получение вебхука от "Мой склад" при изменении "Заказ на производство"
 */
MyWarehouse.setWebHookUpdateProcessingorder = function setWebHookUpdateProcessingorder() {
    this.setWebHook('UPDATE', 'processingorder', this.endpoints.processingorder_update)
};

/**
 * Устанавливает webhook для получения информации
 * об изменении закза покупателя
 */
MyWarehouse.setWebHookCreateOrder = function setWebHookCreateOrder() {
    this.lg.silly('Start setWebHookCreateOrder');
    this.setWebHook("CREATE", "customerorder", this.endpoints.order_create);
};

/**
 * Устанавливает webhook для получения информации
 * об изменении закза покупателя
 */
MyWarehouse.setWebHookChangeOrder = function setWebHookChangeOrder() {
    this.lg.silly('Start setWebHookChangeOrder');
    this.setWebHook("UPDATE", "customerorder", this.endpoints.order_create);
};

/**
 * Устанавливает получение вебхука от "Мой склад" при изменении "Заказ на производство"
 */
MyWarehouse.setWebHookUpdateTask = function setWebHookUpdateTask() {
    this.setWebHook('UPDATE', 'task', this.endpoints.task_update);
};


MyWarehouse.getWebHooks = function getWebHooks() {
    return new Promise((resolve, reject) => {
        const opt = this.getOptions('GET', 'entity/webhook');
        this.send(opt, null, (err, data) => {
            if (err) reject(err);
            resolve(data);
        })
    });
};


MyWarehouse.getOptions = function (method, path) {
    let o = Object.assign({}, this.httpsOptions);
    let mainUrl = `${o.protocol}${o.host}:${o.port}${o.path}/`;
    const url = new URL(path, mainUrl);
    o.path = url.pathname + url.search;
    o.method = method;
    return o;
};


MyWarehouse.deleteAllWebhooks = async function deleteAllWebhooks() {
    const self = this;
    let webhooks;
    try {
        webhooks = await this.getWebHooks();
    } catch (e) {
        this.lg.error(`Cant't get info about webhooks. ${e.message}, CODE: ${e.code}`);
        process.exitCode = 1;
    }

    if (webhooks && webhooks.meta && !webhooks.rows) {
        this.lg.info(`Not one webhooks is set up`);
        return true;
    }
    if (webhooks && webhooks.rows) {
        this.lg.info(`Get ${webhooks.rows.length} existing webhooks`);
    } else {
        return true;
    }


    const listPromises = webhooks.rows.map(function (webhookBody) {
        return new Promise((resolve, reject) => {
            let opt = self.getOptions('DELETE', webhookBody.meta.href);
            self.send(opt, null, (err) => {
                let msg;
                if (err) {
                    msg = `Webhook deleting is fail: "${webhookBody.entityType}" "${webhookBody.action}"`
                        + ` to "${webhookBody.url}"`;
                    self.lg.error(err);
                    reject(msg);
                }
                msg = `Webhook is deleting: "${webhookBody.entityType}" "${webhookBody.action}" to "${
                    webhookBody.meta.href}"`;
                resolve(msg);
            })
        }).then(value => {
            self.lg.info(value)
        }, reason => {
            self.lg.error(reason)
        })
    });

    return Promise.all(listPromises).then(() => true);
};


MyWarehouse.getByHref = function getByHref(href) {
    return new Promise((resolve, reject) => {
        const options = this.getOptions('GET', href);
        this.send(options, null, (err, data) => {
            if (err) reject(err);
            resolve(data);
        });
    });
};

/**
 * @param href
 * @param {Object} payload
 * @return {Promise<*>}
 * @constructor
 */
MyWarehouse.PUT = function PUT(href, payload) {
    const json = JSON.stringify(payload);
    return new Promise((resolve, reject) => {
        const options = this.getOptions('PUT', href);
        this.send(options, json, (err, data) => {
            if (err) reject(err);
            resolve(data);
        });
    });
};

/**
 *
 * @param href
 * @param {Object} payload
 * @return {Promise<*>}
 * @constructor
 */
MyWarehouse.POST = function POST(href, payload) {
    const json = JSON.stringify(payload);
    return new Promise((resolve, reject) => {
        const options = this.getOptions('POST', href);
        this.send(options, json, (err, data) => {
            if (err) reject(err);
            resolve(data);
        });
    });
};


/**
 * Проверяет наличие заказа на производство для заказа покупателя
 *
 * @param {string} numberCustomerOrder Номер заказа покупателя для которого проверяется наличие заказа на производство
 * @return {Promise<boolean>}
 */
MyWarehouse.checkProcessingOrder = async function checkProcessingOrder(numberCustomerOrder) {
    const path = `/api/remap/1.2/entity/processingorder?filter=name~${encodeURIComponent(numberCustomerOrder)}`;
    const body = await this.getByHref(path);
    if (body.meta) {
        return body.meta.size >= 1;
    } else {
        return false;
    }
};

MyWarehouse.getTelegramUserName = require('./getTelegramUsername');

/**
 * Устанавливает webhook для получения информации
 * об изменении закза покупателя
 *
 * @param {string|null} data
 * @param {Object} options
 * @param {function} cb
 */
MyWarehouse.send = function send(options, data, cb) {
    try {
        this.lg.silly(`request to: ${options.url}${options.path}`);
        const req = https.request(options, res => {
            res.setEncoding('utf-8');
            let buffer = [];
            res.on('data', chunk => {
                buffer.push(chunk);
            });
            res.on('error', err => cb(err));
            res.on('end', () => {
                let data;
                if (res.headers &&
                    res.headers['content-type']
                    && res.headers['content-type'].toLowerCase().indexOf('application/json') >= 0
                ) {
                    data = JSON.parse(buffer.join(''));
                    if (data && data.errors) {
                        const myWarehouseEror = new Error(data.errors[0].error);
                        myWarehouseEror.code = data.errors[0].code;
                        cb(myWarehouseEror);
                    }
                } else {
                    data = buffer.join('');
                }
                cb(null, data);
            });
        });
        req.write((data || ''));
        req.end();
        req.on('error', err => {
            this.lg.error(`Can't send request on ${options.protocol}//${options.host}${options.path}\n${err.message}\n${err.code}\n${err.stack}`);
            cb(err);
        })
    } catch (e) {
        cb(e);
    }
};

/**
 *
 * @param {string} startMsg Будет добавлено в начале лога
 * @param {Object[]} errors массив ошибок в формате "Мой склад" описанных по адресу:
 * https://dev.moysklad.ru/doc/api/remap/1.2/#header-%D0%BE%D0%B1%D1%80%D0%B0%D0%B1%D0%BE%D1%82%D0%BA%D0%B0-%D0%BE%D1%88%D0%B8%D0%B1%D0%BE%D0%BA
 * @param logger
 */
MyWarehouse.logErrorMyWarehouse = function (startMsg, errors, logger) {
    (errors || []).forEach(error => {
        logger.error(`${startMsg} [${error.error}] parameter: "${error.parameter}" code: "${error.code}" message: "${error.error_message}"`);
    });
};

/**
 *
 * @param {Object} config
 * @param {Logger} config.lg logger for logging
 * @param {Object} config.httpsOptions options for sending request to My Warehouse
 * @param {string} config.host host for come in webhooks
 * @param {string} config.port Port for come in webhooks
 * @param {Object} config.endpoints paths for come in webhooks
 */
module.exports = MyWarehouse;










