
/**
 *
 * @return {Promise<{id: string, telegram_username: string }[]>}
 */
function getTelegramUserName() {
    this.lg.debug(`Start getTelegramUserName`);
    return new Promise((resolve, reject) => {
        const path = '/entity/employee';
        let opt = Object.assign({}, this.httpsOptions);
        opt.path= opt.path+ path;
        opt.method = 'GET';
        this.send(opt, null, (err, data) => {
            if (err) reject(err);
            const parsedBody = parseUserName(data);
            resolve(parsedBody);
        })
    });
}

/**
 *
 * @param {Object} body
 * @return {{id: string, telegram_username: string}[]}
 */
function parseUserName(body) {
    const employees = body.rows.filter(employee => employee.attributes).map(employee => {
        const e = {id: employee.id};
        const attr = employee.attributes.reduce((chat, curAttr) => {
            if (curAttr.name === 'Telegram') {
                return curAttr;
            } else {
                return chat;
            }
        }, null);
        if (attr && attr.value) {
            e.telegram_username = attr.value;
        }
        return e;
    }).filter(value => value.telegram_username);
    return employees;
}

module.exports = getTelegramUserName;
